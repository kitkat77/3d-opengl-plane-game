#include "main.h"
#include "segment.h"

#ifndef CROSS_H
#define CROSS_H


class Cross {
public:
    Cross() {}
    Cross(float x, float y, float z,color_t color);
    float rotation;
    void draw(glm::mat4 VP);
    Segment targets[5];
    glm::vec3 target;
private:
    VAO* object;
};

#endif
