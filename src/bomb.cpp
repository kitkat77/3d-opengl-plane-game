#include "bomb.h"
#include "main.h"

Bomb::Bomb(float x, float y, float z,color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    this->acc_y = 0.005;
    this->speed_y = -0.5;
    this->is_exist = true;
    this->r = 0.5;
    this->x_r = 1;
    this->y_r = 1;
    this->z_r = 1;
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
    this->bound.width = x_r*r;
    this->bound.height = y_r*r;
    this->bound.depth = z_r*r;
    int n1 = 50, i;
    float pi=3.14;
    GLfloat g_vertex_buffer_data1[10000],temp[10000],g_vertex_buffer_data2[10000];
    for(i=0; i<n1; i++)
    {
        float x = cos(2*pi*i/n1);
        float y = sin(2*pi*i/n1);
        temp[3*i]= x;
        temp[3*i +1]= y;
        temp[3*i +2]= 0.0f;
    }
    
    for(i=0; i<n1; i++)
    {
        //center
        g_vertex_buffer_data1[9*i] = 0.0f;
        g_vertex_buffer_data1[9*i+1] = 0.0f;
        g_vertex_buffer_data1[9*i+2] = -1.0f;
        
        //first vertex
        g_vertex_buffer_data1[9*i+3] = temp[3*i];
        g_vertex_buffer_data1[9*i+4] = temp[3*i+1];
        g_vertex_buffer_data1[9*i+5] = 0.0f;

        //second vertex
        g_vertex_buffer_data1[9*i+6] = temp[(3*i+3)%(3*n1)];
        g_vertex_buffer_data1[9*i+7] = temp[(3*i+4)%(3*n1)];
        g_vertex_buffer_data1[9*i+8] = 0.0f;
        
    }
    for(i=0; i<n1; i++)
    {
        //center
        g_vertex_buffer_data2[9*i] = 0.0f;
        g_vertex_buffer_data2[9*i+1] = 0.0f;
        g_vertex_buffer_data2[9*i+2] = 1.0f;
        
        //first vertex
        g_vertex_buffer_data2[9*i+3] = temp[3*i];
        g_vertex_buffer_data2[9*i+4] = temp[3*i+1];
        g_vertex_buffer_data2[9*i+5] = 0.0f;

        //second vertex
        g_vertex_buffer_data2[9*i+6] = temp[(3*i+3)%(3*n1)];
        g_vertex_buffer_data2[9*i+7] = temp[(3*i+4)%(3*n1)];
        g_vertex_buffer_data2[9*i+8] = 0.0f;
        
    }
    for(i=0; i<n1*9; i++)
    {   
        // x coordinate
        if(i%3 == 0)
        {
            g_vertex_buffer_data1[i] *= x_r;
            g_vertex_buffer_data2[i] *= x_r;
        }

        // y coordinate
        if(i%3 == 1)
        {
            g_vertex_buffer_data1[i] *= y_r;
            g_vertex_buffer_data2[i] *= y_r;
        }

        //z coordinate
        if(i%3 == 2)
        {
            g_vertex_buffer_data1[i] *= z_r;
            g_vertex_buffer_data2[i] *= z_r;
        }
        g_vertex_buffer_data1[i] *= r;
        g_vertex_buffer_data2[i] *= r;
    }
    
    this->object1 = create3DObject(GL_TRIANGLES, n1*3, g_vertex_buffer_data1, color, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, n1*3, g_vertex_buffer_data2, COLOR_RED, GL_FILL);
}

void Bomb::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 1, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(this->position.x,this->position.y,this->position.z));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
}

void Bomb::set_position(float x, float y, float z)
{
    this->position = glm::vec3(x, y, z);
}

void Bomb::tick()
{
    this->position.y += this->speed_y;
    this->speed_y -= this->acc_y;
    this->rotation+=10;
    this->update_bounding_box();
}

void Bomb::update_bounding_box()
{
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
}