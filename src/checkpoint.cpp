#include "checkpoint.h"
#include "main.h"

Checkpoint::Checkpoint(float x, float y, float z,color_t color)
{
    this->r = 4;
    this->position = glm::vec3(x, y-this->r, z);
    this->bound.width = this->r;
    this->bound.height = this->r;
    this->bound.depth = this->r;
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
    this->active = false;
    this->cannons_left = 4;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    /*
    static const GLfloat vertex_buffer_data[] = {
        -this->width, this->thickness, 0.0f,
        this->width, this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        -this->width, this->thickness, 0.0f,
        -this->width, -this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        };
    */
    static const GLfloat vertex_buffer_data[] = {
        -this->r,-this->r,-this->r, // triangle 1 : begin
        -this->r,-this->r, this->r,
        -this->r, this->r, this->r, // triangle 1 : end
        this->r, this->r,-this->r, // triangle 2 : begin
        -this->r,-this->r,-this->r,
        -this->r, this->r,-this->r, // triangle 2 : end
        this->r,-this->r, this->r,
        -this->r,-this->r,-this->r,
        this->r,-this->r,-this->r,
        this->r, this->r,-this->r,
        this->r,-this->r,-this->r,
        -this->r,-this->r,-this->r,
        -this->r,-this->r,-this->r,
        -this->r, this->r, this->r,
        -this->r, this->r,-this->r,
        this->r,-this->r, this->r,
        -this->r,-this->r, this->r,
        -this->r,-this->r,-this->r,
        -this->r, this->r, this->r,
        -this->r,-this->r, this->r,
        this->r,-this->r, this->r,
        this->r, this->r, this->r,
        this->r,-this->r,-this->r,
        this->r, this->r,-this->r,
        this->r,-this->r,-this->r,
        this->r, this->r, this->r,
        this->r,-this->r, this->r,
        this->r, this->r, this->r,
        this->r, this->r,-this->r,
        -this->r, this->r,-this->r,
        this->r, this->r, this->r,
        -this->r, this->r,-this->r,
        -this->r, this->r, this->r,
        this->r, this->r, this->r,
        -this->r, this->r, this->r,
        this->r,-this->r, this->r
        };
    
    this->object = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data, color , GL_FILL);
}

void Checkpoint::draw(glm::mat4 VP)
{
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (0 * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Checkpoint::set_position(float x, float y)
{
    this->position = glm::vec3(x, y, 0);
}

void Checkpoint::tick()
{
        
}
