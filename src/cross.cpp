#include "cross.h"
#include "main.h"
#include "segment.h"

Cross::Cross(float x, float y, float z,color_t color) {
    //this->target = glm::vec3(x, y, z);
    this->target = glm::vec3(x,y,z);
    this->rotation = 0;
    float w=1.5;
    this->targets[0] = Segment(0,0,0,w,COLOR_WHITE);
    this->targets[1] = Segment(0,0,90,w,COLOR_WHITE);
    this->targets[2] = Segment(0,0,45,w,COLOR_BLACK);
    this->targets[3] = Segment(0,0,-45,w,COLOR_BLACK);
    this->targets[0].position.z = this->target.z;
    this->targets[1].position.z = this->target.z;
    this->targets[2].position.z = this->target.z;
    this->targets[3].position.z = this->target.z;
}

void Cross::draw(glm::mat4 VP)
{    
    /*
    float w=1.5;
    this->targets[0] = Segment(this->target.x,this->target.y,0,w,COLOR_WHITE);
    this->targets[1] = Segment(this->target.x,this->target.y,90,w,COLOR_WHITE);
    this->targets[2] = Segment(this->target.x,this->target.y,45,w,COLOR_WHITE);
    this->targets[3] = Segment(this->target.x,this->target.y,-45,w,COLOR_WHITE);
    this->targets[0].position.z = this->target.z;
    this->targets[1].position.z = this->target.z;
    this->targets[2].position.z = this->target.z;
    this->targets[3].position.z = this->target.z;
*/
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->target);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(this->position.x,this->position.y,this->position.z));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    this->targets[0].draw(MVP);
    this->targets[1].draw(MVP);
    this->targets[2].draw(MVP);
    this->targets[3].draw(MVP);
}
