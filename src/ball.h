#include "main.h"
#include "cross.h"

#ifndef BALL_H
#define BALL_H


class Ball {
public:
    Ball() {}
    Ball(float x, float y, float z,color_t color);
    glm::vec3 position;
    glm::vec3 angle;
    float rotation;
    float r;
    float initial_y;
    float x_step_length;
    float y_step_length;
    float z_step_length;
    float x_r; // to scale x part 
    float y_r; // to scale y part
    float z_r; // to scale z part
    float acc_y;
    float acc_x;
    float speed_x;
    float speed_y;
    int cur_jump_state;
    bool on_checkpoint;
    bool speed_increase;
    float fuel;
    float fuel_max;
    float health;
    void draw(glm::mat4 VP);
    void set_position(float x, float y,float z);
    void tick();
    void right_click();
    void left_click();
    void space_click();
    void up_click();
    void down_click();
    void tilt_right();
    void tilt_left();
    void yaw_counterclockwise();
    void yaw_clockwise();
    bounding_box_t bound;
    void update_bounding_box();
    void move_target_y(float d);
    void move_target_z(float d);
    double speed;
    Cross missile_target;

private:
    VAO *object1;
    VAO* object2;
    VAO* object3;
    VAO* object4;
    VAO* object5;
    VAO* object6;
    VAO* object; 
};

#endif // BALL_H
