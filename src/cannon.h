#include "main.h"

#ifndef CANNON_H
#define CANNON_H


class Cannon {
public:
    Cannon() {}
    Cannon(float x, float y,float z,float gx,float gy,float gz,float r,color_t color);
    glm::vec3 position;
    float r;
    int tick_count;
    bool shooting;
    bool is_exist;
    bounding_box_t bound;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
private:
    VAO *object;
};

#endif // CANNON_H
