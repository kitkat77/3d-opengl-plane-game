#include "main.h"

#ifndef SEGMENT_H
#define SEGMENT_H


class Segment {
public:
    Segment() {}
    Segment(float x, float y, float rotation,float width,color_t color);
    glm::vec3 position;
    glm::vec3 axis;
    glm::vec3 axis_point;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    //void set_rotation(glm::vec3 axis,float angle,glm::vec3 axis_point);
    void tick();
    float width;
    float thickness;
    float rotation2;
    bounding_box_t bound;
private:
    VAO *object;
};

#endif // SEGMENT_H
