#include "ball.h"
#include "main.h"
#include "cross.h"

Ball::Ball(float x, float y, float z,color_t color) {
    this->position = glm::vec3(x, y, z);
    this->missile_target = Cross(this->position.x,this->position.y+2,this->position.z,COLOR_WHITE);
    this->missile_target.target = glm::vec3(0,-10,-20);
    this->angle = glm::vec3(0,0,0);
    this->on_checkpoint=false;
    this->fuel_max = 100;
    this->fuel = fuel_max;
    this->rotation = 0;
    this->acc_y = 0.01;
    this->speed_y = 0;
    this->cur_jump_state=0;
    this->speed_x = 0;
    this->acc_x = 0;
    this->initial_y = y;
    this->health = 100;
    speed = 1;
    this->r = 1;
    this->x_r = 1;
    this->y_r = 1;
    this->z_r = 1;
    this->x_step_length = 0.1;
    this->y_step_length = 0.5;
    this->z_step_length = 0.01;
    float length = 2;
    this->bound.depth = length+1;
    this->bound.height = 1;
    this->bound.width = 1;
    this->speed_increase = false;
    float pi = 3.14;
    int n1=12,i,n2=12,n3=1;
    GLfloat g_vertex_buffer_data1[n1*9],temp[1000],g_vertex_buffer_data2[n2*9],g_vertex_buffer_data[1000];
    GLfloat g_vertex_buffer_data5[] =
    {
        0.0f,0.0f,length+1, // triangle 1 : begin
        1.5f,0.0f,length+1,
        0.1f,0.0f,length, // triangle 1 : end
    };
    GLfloat g_vertex_buffer_data6[] =
    {
        0.0f,0.0f,length+1, // triangle 1 : begin
        -1.5f,0.0f,length+1,
        -0.1f,0.0f,length, // triangle 1 : end
    };

    GLfloat g_vertex_buffer_data3[] =
    {
        0.0f,0.0f,0.0f, // triangle 1 : begin
        0.0f,0.0f, 2.0f,
        4.0f,0.0f, 2.0f, // triangle 1 : end
    };

    GLfloat g_vertex_buffer_data4[] =
    {
        0.0f,0.0f,0.0f, // triangle 1 : begin
        0.0f,0.0f, 2.0f,
        -4.0f,0.0f, 2.0f, // triangle 1 : end
    };

    int k;
    for(k=0; k<3; k++)
        g_vertex_buffer_data3[3*k+2] -= 1;
    for(k=0; k<3; k++)
        g_vertex_buffer_data4[3*k+2] -= 1;
    float s=1;
    for(i=0; i<n1; i++)
    {
        float x = s * cos(2*pi*i/n1);
        float y = s * sin(2*pi*i/n1);
        temp[3*i]= x;
        temp[3*i +1]= y;
        temp[3*i +2]= 0.0f;
    }
    for(i=0; i<n1; i++)
    {
        //center
        g_vertex_buffer_data[9*i] = temp[3*i];
        g_vertex_buffer_data[9*i+1] = temp[3*i+1];
        g_vertex_buffer_data[9*i+2] = length;
        
        //first vertex
        g_vertex_buffer_data[9*i+3] = temp[3*i];
        g_vertex_buffer_data[9*i+4] = temp[3*i+1];
        g_vertex_buffer_data[9*i+5] = -length;

        //second vertex
        g_vertex_buffer_data[9*i+6] = temp[(3*i+3)%(6*n1)];
        g_vertex_buffer_data[9*i+7] = temp[(3*i+4)%(6*n1)];
        g_vertex_buffer_data[9*i+8] = -length;
    }
    int j=0;
        
    for(i=n1; i<2*n1; i++)
    {
        //center
        g_vertex_buffer_data[9*i] = temp[(3*j+3)%(6*n1)];
        g_vertex_buffer_data[9*i+1] = temp[(3*j+4)%(6*n1)];
        g_vertex_buffer_data[9*i+2] = -length;
        
        //first vertex
        g_vertex_buffer_data[9*i+3] = temp[3*j];
        g_vertex_buffer_data[9*i+4] = temp[3*j+1];
        g_vertex_buffer_data[9*i+5] = length;

        //second vertex
        g_vertex_buffer_data[9*i+6] = temp[(3*j+3)%(6*n1)];
        g_vertex_buffer_data[9*i+7] = temp[(3*j+4)%(6*n1)];
        g_vertex_buffer_data[9*i+8] = length;
        j++;
    }
    for(i=0; i<n1; i++)
    {
        //center
        g_vertex_buffer_data1[9*i] = 0.0f;
        g_vertex_buffer_data1[9*i+1] = 0.0f;
        g_vertex_buffer_data1[9*i+2] = -1.0f;
        
        //first vertex
        g_vertex_buffer_data1[9*i+3] = temp[3*i];
        g_vertex_buffer_data1[9*i+4] = temp[3*i+1];
        g_vertex_buffer_data1[9*i+5] = 0.0f;

        //second vertex
        g_vertex_buffer_data1[9*i+6] = temp[(3*i+3)%(3*n1)];
        g_vertex_buffer_data1[9*i+7] = temp[(3*i+4)%(3*n1)];
        g_vertex_buffer_data1[9*i+8] = 0.0f;
        
    }

    for(i=0; i<n2; i++)
    {
        //center
        g_vertex_buffer_data2[9*i] = 0.0f;
        g_vertex_buffer_data2[9*i+1] = 0.0f;
        g_vertex_buffer_data2[9*i+2] = 1.0f;
        
        //first vertex
        g_vertex_buffer_data2[9*i+3] = temp[3*i];
        g_vertex_buffer_data2[9*i+4] = temp[3*i+1];
        g_vertex_buffer_data2[9*i+5] = 0.0f;

        //second vertex
        g_vertex_buffer_data2[9*i+6] = temp[(3*i+3)%(3*n1)];
        g_vertex_buffer_data2[9*i+7] = temp[(3*i+4)%(3*n1)];
        g_vertex_buffer_data2[9*i+8] = 0.0f;
        
    }

    for(i=0; i<n1*9; i++)
    {   
        // x coordinate
        if(i%3 == 0)
        {
            g_vertex_buffer_data1[i] *= x_r;
            g_vertex_buffer_data2[i] *= x_r;
            g_vertex_buffer_data[i] *= x_r;
        }

        // y coordinate
        if(i%3 == 1)
        {
            g_vertex_buffer_data1[i] *= y_r;
            g_vertex_buffer_data2[i] *= y_r;
            g_vertex_buffer_data[i] *= y_r;
        }

        //z coordinate
        if(i%3 == 2)
        {
            g_vertex_buffer_data1[i] -= length;
            g_vertex_buffer_data2[i] += length;
            g_vertex_buffer_data[i] *= z_r;
            g_vertex_buffer_data1[i] *= z_r;
            g_vertex_buffer_data2[i] *= z_r;
        }
        g_vertex_buffer_data1[i] *= r;
        g_vertex_buffer_data2[i] *= r;
        g_vertex_buffer_data[i] *= r;
    }
    
    for(i=0; i<n3*3; i++)
    {
        g_vertex_buffer_data3[i] *= r;
        g_vertex_buffer_data4[i] *= r;
    }
    this->object1 = create3DObject(GL_TRIANGLES, n1*3, g_vertex_buffer_data1, color, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, n2*3, g_vertex_buffer_data2, color, GL_FILL);
    this->object3 = create3DObject(GL_TRIANGLES, n3*3, g_vertex_buffer_data3, COLOR_BLACK, GL_FILL);
    this->object4 = create3DObject(GL_TRIANGLES, n3*3, g_vertex_buffer_data4, COLOR_BLACK, GL_FILL);
    this->object5 = create3DObject(GL_TRIANGLES, n3*3, g_vertex_buffer_data5, COLOR_BLACK, GL_FILL);
    this->object6 = create3DObject(GL_TRIANGLES, n3*3, g_vertex_buffer_data6, COLOR_BLACK, GL_FILL);
    this->object = create3DObject(GL_TRIANGLES, n1*3*2, g_vertex_buffer_data, COLOR_WHITE, GL_FILL);
}

void Ball::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    glm::mat4 rotate_x  = glm::rotate((float) (this->angle.x * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 rotate_y  = glm::rotate((float) (this->angle.y * M_PI / 180.0f), glm::vec3(0, 1, 0));
    glm::mat4 rotate_z  = glm::rotate((float) (this->angle.z * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(this->position.x,this->position.y,this->position.z));
    Matrices.model *= (translate * rotate_y * rotate_x *rotate_z);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    draw3DObject(this->object3);
    draw3DObject(this->object5);
    draw3DObject(this->object6);
    draw3DObject(this->object4);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
    this->missile_target.draw(MVP);
}

void Ball::set_position(float x, float y, float z)
{
    this->position = glm::vec3(x, y, z);
}

void Ball::tick()
{
    /*int screen_left = -4;
    int screen_right = 3;
    int screen_down = -3;
    int screen_up = 3;
 
    if(this->position.x+this->r<=screen_right && this->position.x-this->r>=screen_left)
    {
        this->position.x += this->speed_x;        
    }
    else
    {
        this->speed_x=0;
    }
    if(this->position.y-this->r >= screen_down)
    {
        if(!this->cur_jump_state)
        {
            this->speed_y-=this->acc_y;    
        }
        this->position.y += this->speed_y;
    
    }
    else
    {
        this->speed_y=0;
    }
    if(this->position.y-this->r<screen_down)
    {
        this->position.y=screen_down+this->r;
        this->speed_y=0;
    }
        
    this->cur_jump_state = 0;
    printf("%f\n",this->position.y);*/
    float sint = sin(this->angle.y*M_PI/180.0);
    float cost = cos(this->angle.y*M_PI/180.0);
    if(this->z_step_length>0.02 && !this->speed_increase)
        this->z_step_length -= 0.001;
    this->speed_increase=false;
    this->position.z -= this->z_step_length*cost;
    this->position.x -= this->z_step_length*sint;
    this->fuel -= 0.02;
    this->update_bounding_box();
//    this->angle.z=0;
}

void Ball::update_bounding_box()
{
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
//    this->missile_target.target = glm::vec3(0,-20,-40);
    
//    this->bound.height = this->position.y;
//    this->bound.width = this->position.y;
//    this->bound.depth = this->position.y;
    

}
void Ball::left_click()
{
    float sint = sin(this->angle.y*M_PI/180.0);
    float cost = cos(this->angle.y*M_PI/180.0);

    this->position.x -= this->x_step_length*cost;
    this->position.z += this->x_step_length*sint;
}

void Ball::right_click()
{
    float sint = sin(this->angle.y*M_PI/180.0);
    float cost = cos(this->angle.y*M_PI/180.0);

    this->position.x += this->x_step_length*cost;
    this->position.z -= this->x_step_length*sint;
}

void Ball::space_click()
{
    this->speed_y += this->acc_y;
    if(this->speed_y == this->acc_y)
    {
        this->position.y += this->speed_y;    
    }
    this->cur_jump_state=1;
}

void Ball::up_click()
{
    if(this->position.y>117)
        return;
    this->position.y += this->y_step_length;
}

void Ball::down_click()
{
    if(this->on_checkpoint)
        return;
    // if its at sea level
//    if(this->position.y-this->y_step_length<this->initial_y+0.1)
//        return;
    this->position.y -= this->y_step_length;
}

void Ball::tilt_right()
{
    this->angle.z -= 1;
    if(this->angle.z < 360)
        this->angle.z += 360; 
}

void Ball::tilt_left()
{
    this->angle.z += 1;
    if(this->angle.z > 360)
        this->angle.z -= 360;
}

void Ball::yaw_clockwise()
{
    this->angle.y -= 1;
    if(this->angle.y < 360)
        this->angle.y += 360; 
}

void Ball::yaw_counterclockwise()
{
    this->angle.y += 1;
    if(this->angle.y > 360)
        this->angle.y -= 360;
}

void Ball::move_target_y(float d)
{
    this->missile_target.target.y += d;
}

void Ball::move_target_z(float d)
{
    this->missile_target.target.z += d;
}