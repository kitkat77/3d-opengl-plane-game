#include "main.h"
#include "segment.h"

#ifndef BARDISPLAY_H
#define BARDISPLAY_H


class Bardisplay
{
public:
    Bardisplay() {}
    Bardisplay(float x, float y, float z, float rotation,int num_seg,color_t color);
    glm::vec3 position;
    glm::vec3 eye;
    float rotation;
    int num_seg;
    void init_segments(glm::vec3 eye,color_t color);
    void draw(glm::mat4 VP,float alt, glm::vec3 eye,float angle_y);
    void set_position(float x, float y);
    void tick();
    float width;
    float thickness;
    Segment fuel[5];
    Segment fuel2[5];
    bounding_box_t bound;
private:
    VAO *object;
};

#endif // BARSDISPLAY_H
