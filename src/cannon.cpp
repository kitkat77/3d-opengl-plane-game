#include "cannon.h"
#include "main.h"

Cannon::Cannon(float x, float y, float z,float gx,float gy,float gz,float r,color_t color)
{
    this->r = r;
    this->position = glm::vec3(x, y, z);
    this->bound.width = this->r;
    this->bound.height = this->r;
    this->bound.depth = this->r;
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
    this->tick_count = 0 ;
    this->shooting = false;
    this->is_exist = true;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    /*
    static const GLfloat vertex_buffer_data[] = {
        -this->width, this->thickness, 0.0f,
        this->width, this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        -this->width, this->thickness, 0.0f,
        -this->width, -this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        };
    */
    static const GLfloat vertex_buffer_data[] = {
        -this->r,-this->r,-this->r, // triangle 1 : begin
        -this->r,-this->r, this->r,
        -this->r, this->r, this->r, // triangle 1 : end
        this->r, this->r,-this->r, // triangle 2 : begin
        -this->r,-this->r,-this->r,
        -this->r, this->r,-this->r, // triangle 2 : end
        this->r,-this->r, this->r,
        -this->r,-this->r,-this->r,
        this->r,-this->r,-this->r,
        this->r, this->r,-this->r,
        this->r,-this->r,-this->r,
        -this->r,-this->r,-this->r,
        -this->r,-this->r,-this->r,
        -this->r, this->r, this->r,
        -this->r, this->r,-this->r,
        this->r,-this->r, this->r,
        -this->r,-this->r, this->r,
        -this->r,-this->r,-this->r,
        -this->r, this->r, this->r,
        -this->r,-this->r, this->r,
        this->r,-this->r, this->r,
        this->r, this->r, this->r,
        this->r,-this->r,-this->r,
        this->r, this->r,-this->r,
        this->r,-this->r,-this->r,
        this->r, this->r, this->r,
        this->r,-this->r, this->r,
        this->r, this->r, this->r,
        this->r, this->r,-this->r,
        -this->r, this->r,-this->r,
        this->r, this->r, this->r,
        -this->r, this->r,-this->r,
        -this->r, this->r, this->r,
        this->r, this->r, this->r,
        -this->r, this->r, this->r,
        this->r,-this->r, this->r
        };
    
    this->object = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data, color , GL_FILL);
}

void Cannon::draw(glm::mat4 VP)
{
    if(!this->is_exist)
        return;
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (0 * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Cannon::set_position(float x, float y)
{
    this->position = glm::vec3(x, y, 0);
}

void Cannon::tick()
{
    this->tick_count++;
    if(this->tick_count>=1000)
    {
        this->tick_count=0;
        this->shooting = true;
    }
    else
    {
        this->shooting=false;
    }        
}
