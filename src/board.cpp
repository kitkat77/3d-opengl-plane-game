#include "board.h"
#include "segment.h"
#include "main.h"

Board::Board(float x, float y, float z,float rotation,int num_seg, color_t color)
{
    this->position = glm::vec3(x, y, z);
    this->rotation = rotation;
    this->width = width;
    this->thickness = 0.02;
    this->bound.x = position.x;
    this->bound.y = position.y;
    this->bound.width = this->thickness;
    this->bound.height = this->width;
    this->bound.rotation = this->rotation;
    this->num_seg = num_seg;
    float w = 0.11;
    this->init_segments(this->position);
}


//angle y : angle of yaw of plane
void Board::draw(glm::mat4 VP, float alt,glm::vec3 eye,float angle_y)
{
    
    //angle=-20;
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    //glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    glm::mat4 rotate    = glm::rotate((float) (0 * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_y    = glm::rotate((float) (0 * M_PI / 180.0f), glm::vec3(0,1,0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate2          = rotate2 * glm::translate(this->axis_point);
    Matrices.model *= translate * rotate_y * rotate;
    //Matrices.model *= translate * rotate ;
    glm::mat4 MVP = VP * Matrices.model;
    
 //   printf("%f\n",alt);
    int lines[10][8] = 
    {{0,1,2,3,0,5,6,7},
    {0,0,0,3,0,0,6,0},
    {0,1,0,3,4,5,0,7},
    {0,1,0,3,4,0,6,7},
    {0,0,2,3,4,0,6,0},
    {0,1,2,0,4,0,6,7},
    {0,1,2,0,4,5,6,7},
    {0,1,0,3,0,0,6,0},
    {0,1,2,3,4,5,6,7},
    {0,1,2,3,4,0,6,7}};
    
    int temp = alt;
    if(temp<0)
        temp=0;
    for(int i=0; i<7; i++)
    {
        if(i+1>num_seg)
            return;
        for(int j=1; j<=7; j++)
        {
            if(lines[temp%10][j]!=0)
            {   
                altitude[i][j].draw(MVP);

            }
        }
        temp/=10;
    }
}

void Board::set_position(float x, float y)
{
    this->position = glm::vec3(x, y, 0);
}



void Board::tick()
{
        
}

void Board::init_segments(glm::vec3 eye)
{
    this->eye = eye;
    float x_diff = 0.3,w=0.11;
    float x,y,z;
    x=7*w + 3*x_diff;
    y=0;
    z=0;
    for(int i=0; i<7; i++)
    {
        this->altitude[i][1] = Segment(x,y+2*w,0,w,COLOR_WHITE);
        this->altitude[i][2] = Segment(x-w,y+w,90,w,COLOR_WHITE);
        this->altitude[i][3] = Segment(x+w,y+w,90,w,COLOR_WHITE);
        this->altitude[i][4] = Segment(x,y,0,w,COLOR_WHITE);
        this->altitude[i][5] = Segment(x-w,y-w,90,w,COLOR_WHITE);
        this->altitude[i][6] = Segment(x+w,y-w,90,w,COLOR_WHITE);
        this->altitude[i][7] = Segment(x,y-w*2,0,w,COLOR_WHITE);
        x-=x_diff;  
        int j;
        for(j=1; j<=7; j++)
        {
            this->altitude[i][j].position.z = z;
        //    this->altitude[i][j].rotation2 = angle;
        }
    }

}