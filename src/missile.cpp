#include "missile.h"
#include "main.h"

Missile::Missile(float x, float y, float z,float gx, float gy, float gz,float speed_m,color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    this->is_exist = true;
    glm::vec3 v = glm::vec3(gx-x,gy-y,gz-z);
    float l = length(v);
    this->speed.x = speed_m*v.x/l;
    this->speed.y = speed_m*v.y/l;
    this->speed.z = speed_m*v.z/l;
    this->angle = glm::vec3(90,0,0);
    
    this->angle.x = acos(v.x/l);
    this->angle.y = acos(v.y/l);
    this->angle.z = acos(v.z/l);


    this->r = 0.3;
    this->x_r = 1;
    this->y_r = 1;
    this->z_r = 3;

    this->bound.width = this->r * this->x_r;
    this->bound.height = this->r * this->y_r;
    this->bound.depth = 0.5 * this->r * this->z_r;

    int n1 = 50, i;
    float pi=3.14;
    GLfloat g_vertex_buffer_data1[10000],temp[10000];
    for(i=0; i<n1; i++)
    {
        float x = cos(2*pi*i/n1);
        float y = sin(2*pi*i/n1);
        temp[3*i]= x;
        temp[3*i +1]= y;
        temp[3*i +2]= 0.0f;
    }
    
    for(i=0; i<n1; i++)
    {
        //center
        g_vertex_buffer_data1[9*i] = 0.0f;
        g_vertex_buffer_data1[9*i+1] = 0.0f;
        g_vertex_buffer_data1[9*i+2] = -1.0f;
        
        //first vertex
        g_vertex_buffer_data1[9*i+3] = temp[3*i];
        g_vertex_buffer_data1[9*i+4] = temp[3*i+1];
        g_vertex_buffer_data1[9*i+5] = 0.0f;

        //second vertex
        g_vertex_buffer_data1[9*i+6] = temp[(3*i+3)%(3*n1)];
        g_vertex_buffer_data1[9*i+7] = temp[(3*i+4)%(3*n1)];
        g_vertex_buffer_data1[9*i+8] = 0.0f;
        
    }
    for(i=0; i<n1*9; i++)
    {   
        // x coordinate
        if(i%3 == 0)
        {
            g_vertex_buffer_data1[i] *= x_r;
        }

        // y coordinate
        if(i%3 == 1)
        {
            g_vertex_buffer_data1[i] *= y_r;
        }

        //z coordinate
        if(i%3 == 2)
        {
            g_vertex_buffer_data1[i] *= z_r;
        }
        g_vertex_buffer_data1[i] *= r;
    }
    
    this->object = create3DObject(GL_TRIANGLES, n1*3, g_vertex_buffer_data1, color, GL_FILL);
}

void Missile::draw(glm::mat4 VP)
{
    if(!this->is_exist)
        return;    
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    //glm::mat4 rotate_x  = glm::rotate((float) (this->angle.x * M_PI / 180.0f), glm::vec3(1, 0, 0));
    //glm::mat4 rotate_y  = glm::rotate((float) (this->angle.y * M_PI / 180.0f), glm::vec3(0, 1, 0));
    //glm::mat4 rotate_z  = glm::rotate((float) (this->angle.z * M_PI / 180.0f), glm::vec3(0, 0, 1));
    glm::mat4 rotate_x  = glm::rotate((float) (this->angle.x), glm::vec3(1, 0, 0));
    glm::mat4 rotate_y  = glm::rotate((float) (-this->angle.y), glm::vec3(0, 1, 0));
    glm::mat4 rotate_z  = glm::rotate((float) (-this->angle.z), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(this->position.x,this->position.y,this->position.z));
    Matrices.model *= (translate * rotate_y * rotate_x *rotate_z);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Missile::set_position(float x, float y, float z)
{
    this->position = glm::vec3(x, y, z);
}

void Missile::tick()
{
    this->position.x += this->speed.x;
    this->position.y += this->speed.y;
    this->position.z += this->speed.z;
    this->update_bounding_box();
}

void Missile::update_bounding_box()
{
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;

}