#include "main.h"

#ifndef COMPASS_H
#define COMPASS_H


class Compass {
public:
    Compass() {}
    Compass(float x, float y, float z,color_t color);
    glm::vec3 position;
    glm::vec3 angle;
    void draw(glm::mat4 VP,float angle_y);
    void set_position(float x, float y);
    void tick();
    float width;
    float thickness;
    bounding_box_t bound;
private:
    VAO *object;
    VAO* object1;
};

#endif // Compass_H
