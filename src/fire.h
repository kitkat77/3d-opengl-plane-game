#include "main.h"
#include "flame.h"

#ifndef FIRE_H
#define FIRE_H


class Fire {
public:
    Fire() {}
    Fire(float x, float y,float z,color_t color);
    glm::vec3 position;
    float r;
    bounding_box_t bound;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
    int ticks_left;
    Flame flames1[10];
    Flame flames2[10];
private:
    VAO *object;
};

#endif // Fire_H
