#include "main.h"

#ifndef FUELUP_H
#define FUELUP_H


class Fuelup {
public:
    Fuelup() {}
    Fuelup(float x, float y,float z,color_t color);
    glm::vec3 position;
    float r;
    bool is_exist;
    bounding_box_t bound;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
private:
    VAO *object;
};

#endif // Fuelup_H
