#include "main.h"

#ifndef ARROW_H
#define ARROW_H


class Arrow {
public:
    Arrow() {}
    Arrow(float x, float y, float z,color_t color);
    glm::vec3 position;
    glm::vec3 angle;
    void draw(glm::mat4 VP,glm::vec3 cur,glm::vec3 g);
    void set_position(float x, float y);
    void tick();
    float width;
    float thickness;
    bounding_box_t bound;
private:
    VAO *object;
};

#endif // Arrow_H
