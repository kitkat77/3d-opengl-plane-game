#include "main.h"

#ifndef VOLCANO_H
#define VOLCANO_H


class Volcano {
public:
    Volcano() {}
    Volcano(float x, float y,float z,color_t color);
    glm::vec3 position;
    float r;
    bounding_box_t bound;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
private:
    VAO *object;
    VAO *object1;
};

#endif // Volcano_H
