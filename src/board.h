#include "main.h"
#include "segment.h"

#ifndef BOARD_H
#define BOARD_H


class Board
{
public:
    Board() {}
    Board(float x, float y, float z, float rotation,int num_seg,color_t color);
    glm::vec3 position;
    glm::vec3 eye;
    float rotation;
    int num_seg;
    void init_segments(glm::vec3 eye);
    void draw(glm::mat4 VP,float alt, glm::vec3 eye,float angle_y);
    void set_position(float x, float y);
    void tick();
    float width;
    float thickness;
    Segment altitude[7][8];
    //Segment speed[7][8];
    bounding_box_t bound;
private:
    VAO *object;
};

#endif // SEGMENT_H
