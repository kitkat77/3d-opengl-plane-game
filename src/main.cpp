#include "main.h"
#include "timer.h"
#include "ball.h"
#include "segment.h"
#include "board.h"
#include "sea.h"
#include "checkpoint.h"
#include "bardisplay.h"
#include "missile.h"
#include "bomb.h"
#include "cross.h"
#include "cannon.h"
#include "fire.h"
#include "arrow.h"
#include "compass.h"
#include "volcano.h"
#include "fuelup.h"

using namespace std;
GLMatrices Matrices,Matrices2;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Ball ball1;
Board altitude_board;
Board speed_board;
Bardisplay fuel_board;
Bardisplay health_board;
Board score_board;
Sea surface;
Cross missile_target;
vector<Checkpoint> checkpoints;
Checkpoint active_ch;
vector<Missile> missiles;
vector<Missile> shots;
vector<Bomb> bombs;
vector<Cannon> cannons;
vector<Fire> fires;
vector<Volcano> volcanos;
vector<Fuelup> fuelups;
Arrow direction_arrow;
Compass direction_compass;

float surface_level = -3;
int FOLLOW_CAM_VIEW = 1;
int TOP_VIEW = 2;
int TOWER_VIEW = 3;
int PLANE_VIEW = 4;
int HELICOPTER_VIEW = 5;
int score = 0;

int active_index = 0;


double cursor_x,cursor_y;
float screen_zoom = 107, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;
int altitude_digits = 7;
float target_x,target_y,target_z;
float eye_x,eye_y,eye_z;
float up_x=0,up_y=1,up_z=0;
int eye_mode=1;
int mode_tick = 0;
int bomb_tick = 0;
int missile_tick = 0;

Timer t60(1.0 / 60);

// to set the target and eye values according to FOLLOW CAM mode
void set_eye_follow()
{
    up_x = 0;
    up_y = 1;
    up_z = 0;
    target_x = ball1.position.x;  
    target_y = ball1.position.y+2;
    target_z = ball1.position.z;

    eye_x = ball1.position.x + 6*sin(ball1.angle.y*M_PI/180.0);
    eye_y = ball1.position.y+5;
    eye_z = ball1.position.z + 6*cos(ball1.angle.y*M_PI/180.0);
}


void set_eye_tower()
{
    up_x = 0;
    up_y = 1;
    up_z = 0;

    glm::vec3 tos;
    tos.x = 0;
    tos.y = 0;
    tos.z = -10;

    target_x = ball1.position.x + tos.z*sin(ball1.angle.y*M_PI/180.0)+tos.x*cos(ball1.angle.y*M_PI/180.0);
    target_y = ball1.position.y + tos.y;
    target_z = ball1.position.z + tos.z*cos(ball1.angle.y*M_PI/180.0)-tos.x*sin(ball1.angle.y*M_PI/180.0);
        
    // x = p.x - 5
    // y = p.y
    // z = p.z
    glm::vec3 os;
    os.x = -20;
    os.y = 15;
    os.z = -10;

    eye_x = ball1.position.x + os.z*sin(ball1.angle.y*M_PI/180.0)+os.x*cos(ball1.angle.y*M_PI/180.0);
    eye_y = ball1.position.y + os.y;
    eye_z = ball1.position.z + os.z*cos(ball1.angle.y*M_PI/180.0)-os.x*sin(ball1.angle.y*M_PI/180.0);
            

}

void set_eye_plane()
{
    // rotating up by angle.z
    up_x = -sin(ball1.angle.z*M_PI/180.0);
    up_y = cos(ball1.angle.z*M_PI/180.0);
    up_z = 0;

    glm::vec3 tos;
    tos.x = 0;
    tos.y = 0;
    tos.z = -6;

    target_x = ball1.position.x + tos.z*sin(ball1.angle.y*M_PI/180.0)+tos.x*cos(ball1.angle.y*M_PI/180.0);
    target_y = ball1.position.y + tos.y;
    target_z = ball1.position.z + tos.z*cos(ball1.angle.y*M_PI/180.0)-tos.x*sin(ball1.angle.y*M_PI/180.0);
        
    // x = p.x - 5
    // y = p.y
    // z = p.z
    glm::vec3 os;
    os.x = 0;
    os.y = 0;
    os.z = -3;

    eye_x = ball1.position.x + os.z*sin(ball1.angle.y*M_PI/180.0)+os.x*cos(ball1.angle.y*M_PI/180.0);
    eye_y = ball1.position.y + os.y;
    eye_z = ball1.position.z + os.z*cos(ball1.angle.y*M_PI/180.0)-os.x*sin(ball1.angle.y*M_PI/180.0);
            

}

void set_eye_top()
{
    up_x = 0;
    up_y = 1;
    up_z = 0;

    target_x = ball1.position.x;  
    target_y = ball1.position.y;
    target_z = ball1.position.z;
    /*
    glm::mat4 rotate_y  = glm::rotate((float) (ball1.angle.y * M_PI / 180.0f), glm::vec3(0, 1, 0));
    rotate_y = rotate_y * glm::translate(glm::vec3(ball1.position.x,ball1.position.y,ball1.position.z));
    
    eye_x = ball1.position.x + 6*sin(ball1.angle.y*M_PI/180.0);
    eye_y = ball1.position.y+5;
    eye_z = ball1.position.z + 6*cos(ball1.angle.y*M_PI/180.0);
    */
    eye_x = ball1.position.x;
    eye_y = ball1.position.y +20;
    eye_z = ball1.position.z+1;
}

void set_eye_helicopter()
{
    up_x = 0;
    up_y = 1;
    up_z = 0;

    target_x = ball1.position.x;  
    target_y = ball1.position.y;
    target_z = ball1.position.z;
    cursor_x -= 325;
    cursor_y -= 325;
    cursor_x/=325;
    cursor_y/=325;
    cursor_x*=4;
    cursor_y*=4;

    
    eye_x = ball1.position.x+cursor_x;
    eye_y = ball1.position.y +10;
    eye_z = ball1.position.z+1+ cursor_y;
}

// drawing altitude; 7-segment display
void draw_altitude(glm::mat4 VP)
{
    altitude_board.draw(VP,ball1.position.y,glm::vec3(eye_x,eye_y,eye_z),0);   
}

void draw_speed(glm::mat4 VP)
{
    speed_board.draw(VP,100*ball1.z_step_length,glm::vec3(eye_x,eye_y,eye_z),0);   
}

void draw_fuel(glm::mat4 VP)
{
    fuel_board.draw(VP,ball1.fuel,glm::vec3(eye_x,eye_y,eye_z),0);
}

void draw_health(glm::mat4 VP)
{
    health_board.draw(VP,ball1.health,glm::vec3(eye_x,eye_y,eye_z),0);
}

void draw_direction_arrow(glm::mat4 VP)
{
    glm::vec3 os;
    os.x = 4;
    os.y = 1;
    os.z = 0;

    if(eye_mode == PLANE_VIEW)
    {
        os.x = 3;
        os.y = -2;
        os.z = -8;

    } 

    glm::vec3 v;
    v.x = ball1.position.x + os.z*sin(ball1.angle.y*M_PI/180.0)+os.x*cos(ball1.angle.y*M_PI/180.0);
    v.y = ball1.position.y + os.y;
    v.z = ball1.position.z + os.z*cos(ball1.angle.y*M_PI/180.0)-os.x*sin(ball1.angle.y*M_PI/180.0);

    direction_arrow.position = v;
    direction_arrow.draw(VP,ball1.position,checkpoints[active_index].position);

}

/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw()
{
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - glGetUniformLocationation of camera. Don't change unless you are sure!!
    glm::vec3 eye_first ( eye_x , eye_y, eye_z);
    
    glm::vec3 eye_top ( eye_x, eye_y, eye_z);
    
    glm::vec3 eye_tower ( 10, 12, 0);
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target (0, -3, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!

    glm::vec3 up (up_x, up_y, up_z);

//    if(eye_mode == TOP_VIEW)
//        glm::vec3 up (0, 0, 1);

    // Compute Camera matrix (Vieww)
    //glm::vec3 target2( ball1.position.x + 3*cos(ball1.rotation*M_PI/180.0), ball1.position.y + 3*sin(ball1.rotation*M_PI/180.0), ball1.position.z + 1);;
    glm::vec3 target2( target_x,target_y,target_z);;
    //glm::vec3 target2( 0 , 0, 0);;
//    glm::vec3 eye_temp( ball1.position.x,ball1.position.y+20,ball1.position.z+1);
//    glm::vec3 target_temp(ball1.position.x,ball1.position.y,ball1.position.z);
    
    Matrices.view = glm::lookAt( eye_first, target2, up ); // Rotating Camera for 3D    
    
    // Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
//    glm::mat4 rotate_y  = glm::rotate((float) (ball1.angle.y * M_PI / 180.0f), glm::vec3(0, 1, 0));
//    rotate_y = rotate_y * glm::translate(glm::vec3(ball1.position.x,ball1.position.y,ball1.position.z));
//    Matrices.model = glm::mat4(1.0f);
//    Matrices.model *= rotate_y;
    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model

    glm::vec3 eye_d ( 0, 0, 100);
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target_d (0, 0, 98);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!

    glm::vec3 up_d (0, 1, 0);

    Matrices2.view = glm::lookAt( eye_d, target_d, up_d ); // Rotating Camera for 3D    
    glm::mat4 VP2 = Matrices2.projection * Matrices2.view;
    


    // Scene render
    ball1.draw(VP);
    draw_altitude(VP2);
    draw_speed(VP2);
    draw_fuel(VP2);
    draw_health(VP2);
    score_board.draw(VP2,score,glm::vec3(eye_x,eye_y,eye_z),0);

    draw_direction_arrow(VP);
    direction_compass.draw(VP2,ball1.angle.y);
    surface.draw(VP);
    for(int i=0; i<checkpoints.size(); i++)
    {
        checkpoints[i].draw(VP);
    }
    for(int i=0; i<missiles.size(); i++)
    {
        missiles[i].draw(VP);
    }
    for(int i=0; i<bombs.size(); i++)
    {
        bombs[i].draw(VP);
    }
    for(int i=0; i<cannons.size(); i++)
    {
        cannons[i].draw(VP);
    }
    for(int i=0; i<shots.size(); i++)
    {
        shots[i].draw(VP);
    }

    for(int i=0; i<fires.size(); i++)
    {

     //   printf("before drawing\n");
        fires[i].draw(VP);
    }
    for(int i=0; i<volcanos.size(); i++)
    {
        volcanos[i].draw(VP);
    }
    for(int i=0; i<fuelups.size(); i++)
    {
        fuelups[i].draw(VP);
    }

}

void create_cannons()
{
    glm::vec3 p = active_ch.position;
    float r = active_ch.r;
    r=r/2;
    Cannon c1 = Cannon(p.x+r,p.y+active_ch.r,p.z-r,ball1.position.x,ball1.position.y,ball1.position.z,1,COLOR_GREY);
    c1.tick_count = 100;
    cannons.push_back(c1);
    
    c1 = Cannon(p.x+r,p.y+active_ch.r,p.z+r,ball1.position.x,ball1.position.y,ball1.position.z,1.2,COLOR_GREY);
    c1.tick_count = 200;
    cannons.push_back(c1);
    c1 = Cannon(p.x-r,p.y+active_ch.r,p.z+r,ball1.position.x,ball1.position.y,ball1.position.z,1,COLOR_GREY);
    c1.tick_count = 300;
    cannons.push_back(c1);
    c1 = Cannon(p.x-r,p.y+active_ch.r,p.z-r,ball1.position.x,ball1.position.y,ball1.position.z,0.8,COLOR_GREY);
    c1.tick_count = 400;
    cannons.push_back(c1);
    
}

void create_fuelup()
{
    int r1 = rand();
    r1%=600;
    r1-=300;
    int r2 = rand();
    r2%=600;
    r2-=300;
    int x=10;
    while(x>0)
    {
        Fuelup fu = Fuelup(r1,17,r2,COLOR_DARK_ORANGE);
        int i;
        for(i=0; i<fuelups.size(); i++)
        {
            Fuelup fi = fuelups[i];
            if(!fi.is_exist)
                continue;
            fi.bound.width+=50;
            fi.bound.depth+=50;
            if(detect_collision(fi.bound,fu.bound))
            {
                x--;
                break;
            }
        }
        if(i==fuelups.size())
        {
            fuelups.push_back(fu);
            return;
        }
    }    
}

void check_all_collisions()
{
    if(detect_collision(ball1.bound,surface.bound))
    {
        printf("\n");
        printf("--------------------------------------------\n\n");
        printf("You fell into the sea :(. Game over.\n\n");
        printf("--------------------------------------------\n");
        printf("\n");
        exit(0);

    }
    for(int i=0; i<checkpoints.size(); i++)
    {
        if(detect_collision(ball1.bound,checkpoints[i].bound))
        {
            //printf("collision with checkpoint\n");
            ball1.on_checkpoint=true;
        }
    }

    //check b/w bombs
    for(int i=0; i<bombs.size(); i++)
    {
        Bomb b = bombs[i];
        if(!b.is_exist)
            continue;
        //with cannons
        for(int j=0; j<cannons.size(); j++)
        {

            Cannon c = cannons[j];
            if(!c.is_exist)
                continue;

            if(detect_collision(b.bound,c.bound))
            {
                score+=10;
                bombs[i].is_exist = false;
                cannons[j].is_exist = false;
                Fire f = Fire(c.position.x,c.position.y+c.r,c.position.z,COLOR_ORANGE);
                fires.push_back(f);
                active_ch.cannons_left--;
                if(active_ch.cannons_left==0)
                {
                    active_index++;
                    active_ch = checkpoints[active_index];
                    create_cannons();
                }
            }
        }
        //with checkpoints
        for(int j=0; j<checkpoints.size(); j++)
        {
            Checkpoint c = checkpoints[j];
            if(detect_collision(b.bound,c.bound))
            {
                score+=2;
                bombs[i].is_exist = false;
                Fire f = Fire(b.position.x,b.position.y,b.position.z,COLOR_ORANGE);
                fires.push_back(f);
            }
        }
    }
    for(int i=0; i<missiles.size(); i++)
    {
        Missile m = missiles[i];
        if(!m.is_exist)
                continue;
            
        for(int j=0; j<cannons.size(); j++)
        {
            Cannon c = cannons[j];
            if(!c.is_exist)
                continue;
            if(eye_mode == TOP_VIEW)
            {
                c.bound.height *= 10;    
            }
            
            if(detect_collision(c.bound,m.bound))
            {
                score+=15;
                missiles[i].is_exist = false;
                cannons[j].is_exist = false;
                Fire f = Fire(c.position.x,c.position.y+c.r,c.position.z,COLOR_ORANGE);
                fires.push_back(f);
                active_ch.cannons_left--;
                if(active_ch.cannons_left==0)
                {
                    active_index++;
                    active_ch = checkpoints[active_index];
                    create_cannons();
                }
            }
        }
        for(int j=0; j<checkpoints.size(); j++)
        {
            Checkpoint c = checkpoints[j];
            if(detect_collision(c.bound,m.bound))
            {
                score+=2;
                missiles[i].is_exist = false;
                Fire f = Fire(m.position.x,m.position.y,m.position.z,COLOR_ORANGE);
                fires.push_back(f);
            }

        }
    }
    for(int i=0; i<shots.size(); i++)
    {
        if(!shots[i].is_exist)
            continue;
        if(detect_collision(shots[i].bound,ball1.bound))
        {
            shots[i].is_exist = false;
            ball1.health-=10;
        }
    }
    for(int i=0; i<volcanos.size(); i++)
    {
        if(detect_collision(volcanos[i].bound,ball1.bound))
        {
            printf("\n");
            printf("--------------------------------------------\n\n");
            printf("You ran into a volcano :/. Game over.\n\n");
            printf("--------------------------------------------\n");
            printf("\n");
        
            exit(0);
        }
    }
    for(int i=0; i<fuelups.size(); i++)
    {
        if(!fuelups[i].is_exist)
            continue;
        if(detect_collision(fuelups[i].bound,ball1.bound))
        {
            ball1.fuel = ball1.fuel_max;
            fuelups[i].is_exist = false;
            create_fuelup();
        }
    }

}

void create_bomb()
{
    if(bomb_tick > 0)
        return;
    Bomb bo = Bomb(ball1.position.x,ball1.position.y,ball1.position.z,COLOR_BLACK);
    bombs.push_back(bo);
    bomb_tick = 30;
}

void create_missile()
{
    if(missile_tick > 0)
        return;
    float x,y,z,nx,ny,nz,cost,sint,angle_z,angle_y;
    x=ball1.missile_target.target.x;
    y=ball1.missile_target.target.y;
    z=ball1.missile_target.target.z;
    //x=0;
    //y=-20;
    //z=-40;
    angle_z = ball1.angle.z;
    angle_y = ball1.angle.y;
    
    // first rotate about z axis
    // newx = xcos - ysin
    // new y = xsin + ycos
    cost = cos(angle_z*M_PI/180.0);    
    sint = sin(angle_z*M_PI/180.0);    
    nz = z;
    nx = x*cost - y*sint;
    ny = x*sint + y*cost;
    x=nx; y=ny; z=nz;
    // now rotate about y axis
    // newx = xcos + zsin
    // newz = -xsin + zcos
    cost = cos(angle_y*M_PI/180.0);    
    sint = sin(angle_y*M_PI/180.0);    
    ny = y;
    nx = x*cost + z*sint;
    nz = -x*sint + z*cost;
    x=nx; y=ny; z=nz;
    
    x += ball1.position.x;
    y += ball1.position.y;
    z += ball1.position.z;
    Missile mi = Missile(ball1.position.x,ball1.position.y,ball1.position.z,x,y,z,0.5,COLOR_ORANGE);
    missiles.push_back(mi);
    missile_tick = 15;
}



void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT);
    int space = glfwGetKey(window, GLFW_KEY_SPACE);
    int up = glfwGetKey(window, GLFW_KEY_UP);
    int down = glfwGetKey(window, GLFW_KEY_DOWN);
    int a = glfwGetKey(window, GLFW_KEY_A);
    int d = glfwGetKey(window, GLFW_KEY_D);
    int q = glfwGetKey(window, GLFW_KEY_Q);
    int e = glfwGetKey(window, GLFW_KEY_E);
    int w = glfwGetKey(window, GLFW_KEY_W);
    int z = glfwGetKey(window, GLFW_KEY_Z);
    int p = glfwGetKey(window, GLFW_KEY_P);
    int l = glfwGetKey(window, GLFW_KEY_L);
    int j = glfwGetKey(window, GLFW_KEY_J);
    int k = glfwGetKey(window, GLFW_KEY_K);
    int click_one = glfwGetKey(window, GLFW_KEY_1);
    int click_two = glfwGetKey(window, GLFW_KEY_2);
    int click_three = glfwGetKey(window, GLFW_KEY_3);
    int click_four = glfwGetKey(window, GLFW_KEY_4);
    int click_five = glfwGetKey(window, GLFW_KEY_5);
    int left_click = glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_1);
    int right_click = glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_2);
    glfwGetCursorPos(window, &cursor_x, &cursor_y);

    if (space)
    {
        ball1.space_click();
    }   
    if(left)
    {
        ball1.left_click();
    }
    if(right)
    {
        ball1.right_click();
    }
    if(up)
    {
        ball1.up_click();
    }
    if(down)
    {
        ball1.down_click();
    }
    if(a)
    {
        ball1.tilt_left();
    }
    if(d)
    {
        ball1.tilt_right();
    }
    if(q)
    {
        ball1.yaw_counterclockwise();
    }
    if(e)
    {
        ball1.yaw_clockwise();
    }
    if(w)
    {
        if(ball1.z_step_length<10)
        {
            ball1.z_step_length+=0.01;
            ball1.speed_increase = true;
        }
    }
    if(z)
    {
        if(ball1.z_step_length>0.02)
        {
            ball1.z_step_length-=0.01;
            ball1.speed_increase = true;
        }
        
    }
    if(click_one)
    {
        if(mode_tick==0)
        {
            eye_mode = FOLLOW_CAM_VIEW;
//            mode_tick = 30;
        }
    }
    if(click_two)
    {
        if(mode_tick==0)
        {
            eye_mode = TOP_VIEW;
//            mode_tick = 30;
        }
    }
    if(click_three)
    {
        if(mode_tick==0)
        {
            eye_mode = TOWER_VIEW;
//            mode_tick = 30;
        }
    }
    if(click_four)
    {
        if(mode_tick==0)
        {
            eye_mode = PLANE_VIEW;
//            mode_tick = 30;
        }
    }
    if(click_five)
    {
        if(mode_tick==0)
        {
            eye_mode = HELICOPTER_VIEW;
            set_eye_top();
//            mode_tick = 30;
        }
    }
    
    if(right_click)
    {
        create_bomb();
    }
    if(left_click)
    {
        create_missile();
    }
    if(p)
    {
        ball1.move_target_y(0.1);
    }
    if(l)
    {
        ball1.move_target_y(-0.1);
    }
    if(j)
    {
        ball1.move_target_z(0.1);
    }
    if(k)
    {
        ball1.move_target_z(-0.1);
    }
    update_target_eye();
    ball1.update_bounding_box();

}

void remove_elements()
{
    int num_bomb=0;
    for(int i=0; i<bombs.size(); i++)
    {
        if(bombs[i].position.y > surface_level)
            num_bomb++;
    }
    if(num_bomb==0)
    {
        bombs.clear();
    }
    int num_cannon = 0;
    for(int i=0; i<cannons.size(); i++)
    {
        if(cannons[i].is_exist)
            num_cannon++;
    }
    if(num_cannon==0)
    {
        cannons.clear();
    }
    int num_fires = 0;
    for(int i=0; i<fires.size(); i++)
    {
        if(fires[i].ticks_left > 0)
            num_fires++;
    }
    if(num_fires==0)
        fires.clear();

    int num_missiles = 0;
    for(int i=0; i<missiles.size(); i++)
    {
        if(missiles[i].is_exist && missiles[i].position.y > surface_level)
            num_missiles++;
    }
    if(num_missiles==0)
        missiles.clear();
    int num_shots = 0;
    for(int i=0; i<shots.size(); i++)
    {
        if(shots[i].is_exist && shots[i].position.y < ball1.position.y + 20)
            num_shots++;
    }
    if(num_shots == 0)
        shots.clear();  
    int num_fuelups = 0;
    for(int i=0; i<fuelups.size(); i++)
    {
        if(fuelups[i].is_exist)
            num_fuelups++;
    }
    if(num_fuelups==0)
        fuelups.clear();

}

void check_cannons_shooting()
{
    for(int i=0; i<cannons.size(); i++)
    {

        if(cannons[i].shooting)
        {
            Cannon c = cannons[i];
            float x,y,z;
            x = c.position.x; y = c.position.y; z = c.position.z;
            Missile sh = Missile(x,y,z,ball1.position.x,ball1.position.y,ball1.position.z,0.1,COLOR_RED);
            shots.push_back(sh);
        }
    }
}

void tick_elements()
{
    ball1.tick();
    update_target_eye();
    ball1.on_checkpoint=false;
    if(ball1.fuel == 0)
    {
        printf("\n");
        printf("--------------------------------------------\n\n");
        printf("You ran out of fuel. Game over.\n\n");
        printf("--------------------------------------------\n");
        printf("\n");
         
        exit(0);

    }
    if(ball1.health == 0)
    {
        printf("\n");
        printf("--------------------------------------------\n\n");
        printf("You ran out of health. Game over.\n\n");
        printf("--------------------------------------------\n");
        printf("\n");
         
        exit(0);

    }

    camera_rotation_angle += 1;
    for(int i=0; i<bombs.size(); i++)
    {
        bombs[i].tick();
    }
    for(int i=0; i<missiles.size(); i++)
    {
        missiles[i].tick();
    }
    for(int i=0; i<shots.size(); i++)
    {
        shots[i].tick();
    }
    for(int i=0; i<cannons.size(); i++)
    {
        cannons[i].tick();
    }
    for(int i=0; i<fires.size(); i++)
    {
        fires[i].tick();
    }
    check_all_collisions();
    check_cannons_shooting();
    remove_elements();
    if(mode_tick>0)
        mode_tick--;
    if(bomb_tick>0)
        bomb_tick--;
    if(missile_tick>0)
        missile_tick--;
}

// to update target and values, calls different functions according to viewing mode
void update_target_eye()
{
    if(eye_mode == FOLLOW_CAM_VIEW)
    {
        set_eye_follow();
    }
    if(eye_mode == TOP_VIEW)
    {
        set_eye_top();
    }
    if(eye_mode == TOWER_VIEW)
    {
        set_eye_tower();
    }
    if(eye_mode == PLANE_VIEW)
    {
        set_eye_plane();
    }
    if(eye_mode == HELICOPTER_VIEW)
    {
        set_eye_helicopter();
    }
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models
    float off=100;

    ball1       = Ball(0,-3, 0,COLOR_WHITE);
    surface = Sea(ball1.position.x,ball1.position.y,COLOR_DARK_BLUE);
    altitude_board = Board(-3.7,-3.5,97,0,7,COLOR_WHITE);
    score_board = Board(2,-3.5,97,0,5,COLOR_WHITE);
    speed_board = Board(-4.9,-2.8,97,0,3,COLOR_WHITE);
    fuel_board = Bardisplay(-2.1,-2.8,97,0,5,COLOR_ORANGE);
    health_board = Bardisplay(-2.6,-2.8,97,0,5,COLOR_GREEN);
    Checkpoint ch = Checkpoint(ball1.position.x-1*off,ball1.position.y+2,1*off,COLOR_BROWN);
    checkpoints.push_back(ch);
    ch = Checkpoint(ball1.position.x+off,ball1.position.y+2,off,COLOR_BROWN);
    checkpoints.push_back(ch);
    ch = Checkpoint(ball1.position.x+off,ball1.position.y+2,-off,COLOR_BROWN);
    checkpoints.push_back(ch);
    ch = Checkpoint(ball1.position.x-off,ball1.position.y+2,-off,COLOR_BROWN);
    checkpoints.push_back(ch);
    checkpoints[0].active = true;
    active_ch = checkpoints[0];
    Volcano vo = Volcano(ball1.position.x-10,surface.position.y+2+surface.r,ball1.position.z,COLOR_DARK_ORANGE);
//    volcanos.push_back(vo);
    vo = Volcano(ball1.position.x-off,surface.position.y+2+surface.r,0,COLOR_DARK_ORANGE);
    volcanos.push_back(vo);
    vo = Volcano(ball1.position.x+off,surface.position.y+2+surface.r,0,COLOR_DARK_ORANGE);
    volcanos.push_back(vo);
    vo = Volcano(ball1.position.x,surface.position.y+2+surface.r,off,COLOR_DARK_ORANGE);
    volcanos.push_back(vo);
    vo = Volcano(ball1.position.x,surface.position.y+2+surface.r,-off,COLOR_DARK_ORANGE);
    volcanos.push_back(vo);
    ball1.position.y += 10;
    missile_target = Cross(ball1.position.x,ball1.position.y+2,ball1.position.z,COLOR_WHITE);
    direction_compass = Compass(3,3,97,COLOR_GREEN);
    direction_arrow = Arrow(3,3,97,COLOR_RED);
    
    off/=2;
    Fuelup f = Fuelup(ball1.position.x-off,ball1.position.y+10,ball1.position.z+off,COLOR_ORANGE);
    fuelups.push_back(f);
    f = Fuelup(ball1.position.x-off,ball1.position.y+10,ball1.position.z-off,COLOR_ORANGE);
    fuelups.push_back(f);
    f = Fuelup(ball1.position.x+off,ball1.position.y+10,ball1.position.z+off,COLOR_ORANGE);
    fuelups.push_back(f);
    f = Fuelup(ball1.position.x+off,ball1.position.y+10,ball1.position.z-off,COLOR_ORANGE);
    fuelups.push_back(f);
        



    create_cannons();    


//    missiles.push_back(m);
    //init_segments();

    update_target_eye();
    // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");



    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 650;
    int height = 650;

    window = initGLFW(width, height);

    initGL (window, width, height);
    Matrices.projection = glm::perspective(glm::radians(screen_zoom), (float)600 / (float)600, 0.1f, 100.0f);
    Matrices2.projection = glm::perspective(glm::radians(screen_zoom), (float)600 / (float)600, 0.1f, 100.0f);
    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {
        // Process timers

        if (t60.processTick()) {
            // 60 fps
            // OpenGL Draw commands
            draw();
            // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);

            tick_elements();
            tick_input(window);

        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();
    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x)  < (a.width + b.width)) &&
           (abs(a.y - b.y)  < (a.height + b.height)) &&
            (abs(a.z - b.z)  < (a.depth + b.depth));
}

void reset_screen()
{
    float top    = screen_center_y + 4 / screen_zoom;
    float bottom = screen_center_y - 4 / screen_zoom;
    float left   = screen_center_x - 4 / screen_zoom;
    float right  = screen_center_x + 4 / screen_zoom;
    Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
    Matrices2.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
}
