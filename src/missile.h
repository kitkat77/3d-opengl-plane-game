#include "main.h"

#ifndef MISSILE_H
#define MISSILE_H


class Missile {
public:
    Missile() {}
    Missile(float x, float y, float z,float gx,float gy,float gz,float speed_m,color_t color);
    glm::vec3 position;
    glm::vec3 angle;
    glm::vec3 speed;
    float rotation;
    float r;
    float x_r; // to scale x part 
    float y_r; // to scale y part
    float z_r; // to scale z part
    bool is_exist;
    void draw(glm::mat4 VP);
    void set_position(float x, float y,float z);
    void tick();
    bounding_box_t bound;
    void update_bounding_box();
private:
    VAO* object; 
};

#endif // MISSILE_H
