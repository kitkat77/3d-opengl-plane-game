#include "volcano.h"
#include "main.h"

Volcano::Volcano(float x, float y, float z,color_t color)
{
    this->r = 4;
    this->position = glm::vec3(x, y-this->r, z);
    this->bound.width = this->r;
    this->bound.height = 10*this->r+3;
    this->bound.depth = this->r;
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    /*
    static const GLfloat vertex_buffer_data[] = {
        -this->width, this->thickness, 0.0f,
        this->width, this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        -this->width, this->thickness, 0.0f,
        -this->width, -this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        };
    */
    static const GLfloat vertex_buffer_data[] = {
        -this->r,-this->r,-this->r, // triangle 1 : begin
        -this->r,-this->r, this->r,
        -this->r, this->r, this->r, // triangle 1 : end
        this->r, this->r,-this->r, // triangle 2 : begin
        -this->r,-this->r,-this->r,
        -this->r, this->r,-this->r, // triangle 2 : end
        this->r,-this->r, this->r,
        -this->r,-this->r,-this->r,
        this->r,-this->r,-this->r,
        this->r, this->r,-this->r,
        this->r,-this->r,-this->r,
        -this->r,-this->r,-this->r,
        -this->r,-this->r,-this->r,
        -this->r, this->r, this->r,
        -this->r, this->r,-this->r,
        this->r,-this->r, this->r,
        -this->r,-this->r, this->r,
        -this->r,-this->r,-this->r,
        -this->r, this->r, this->r,
        -this->r,-this->r, this->r,
        this->r,-this->r, this->r,
        this->r, this->r, this->r,
        this->r,-this->r,-this->r,
        this->r, this->r,-this->r,
        this->r,-this->r,-this->r,
        this->r, this->r, this->r,
        this->r,-this->r, this->r,
        this->r, this->r, this->r,
        this->r, this->r,-this->r,
        -this->r, this->r,-this->r,
        this->r, this->r, this->r,
        -this->r, this->r,-this->r,
        -this->r, this->r, this->r,
        this->r, this->r, this->r,
        -this->r, this->r, this->r,
        this->r,-this->r, this->r
        };

    int n1=10,i;
    float pi=3.14;
    float r=this->r;
    float x_r = 1;
    float y_r = 1;
    float z_r = 1;

    GLfloat g_vertex_buffer_data1[10000],temp[10000];
    for(i=0; i<n1; i++)
    {
        float x = cos(2*pi*i/n1);
        float z = sin(2*pi*i/n1);
        temp[3*i]= x;
        temp[3*i +1]= z;
        temp[3*i +2]= 0.0f;
    }
    float yy = 0;
    for(i=0; i<n1; i++)
    {
        //center
        g_vertex_buffer_data1[9*i] = 0.0f;
        g_vertex_buffer_data1[9*i+1] = yy+3;
        g_vertex_buffer_data1[9*i+2] = 0.0f;
        
        //first vertex
        g_vertex_buffer_data1[9*i+3] = temp[3*i];
        g_vertex_buffer_data1[9*i+4] = yy;
        g_vertex_buffer_data1[9*i+5] = temp[3*i+1];

        //second vertex
        g_vertex_buffer_data1[9*i+6] = temp[(3*i+3)%(3*n1)];
        g_vertex_buffer_data1[9*i+7] = yy;
        g_vertex_buffer_data1[9*i+8] = temp[(3*i+4)%(3*n1)];
        
    }
    for(i=0; i<n1*9; i++)
    {   
        // x coordinate
        if(i%3 == 0)
        {
            g_vertex_buffer_data1[i] *= x_r;
        }

        // y coordinate
        if(i%3 == 1)
        {
            g_vertex_buffer_data1[i] *= y_r;
        }

        //z coordinate
        if(i%3 == 2)
        {
            g_vertex_buffer_data1[i] *= z_r;
        }
        g_vertex_buffer_data1[i] *= r;
    }
    
    this->object = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data, COLOR_BROWN , GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES, n1*3, g_vertex_buffer_data1, color, GL_FILL);

}

void Volcano::draw(glm::mat4 VP)
{
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (0 * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object1);
    draw3DObject(this->object);
}

void Volcano::set_position(float x, float y)
{
    this->position = glm::vec3(x, y, 0);
}

void Volcano::tick()
{
        
}
