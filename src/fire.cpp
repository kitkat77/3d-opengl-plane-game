#include "fire.h"
#include "flame.h"
#include "main.h"

Fire::Fire(float x, float y, float z,color_t color)
{
    this->r = 0.15;
    float r = this->r;
    y+=this->r;
    //y+=10*this->r;
    this->position = glm::vec3(x, y, z);
    this->bound.width = this->r;
    this->bound.height = this->r;
    this->bound.depth = this->r;
    this->bound.x = this->position.x;
    this->bound.y = this->position.y;
    this->bound.z = this->position.z;
    this->ticks_left = 100;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    /*
    static const GLfloat vertex_buffer_data[] = {
        -this->width, this->thickness, 0.0f,
        this->width, this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        -this->width, this->thickness, 0.0f,
        -this->width, -this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        };
    */
    color_t color1 = COLOR_ORANGE;
    color_t color2 = COLOR_YELLOW;
    float r2 = r;
    r*=0.75;

    this->flames1[0] = Flame(x-2*r,y,z-2*r,r2,color1);     
    this->flames1[1] = Flame(x,y,z,r2,color1);     
    this->flames1[2] = Flame(x+2*r,y,z+2*r,r2,color1);     
    this->flames1[3] = Flame(x-2*r,y,z+2*r,r2,color1);     
    this->flames1[4] = Flame(x+2*r,y,z-2*r,r2,color1);     

    y += 2*this->r;
    this->flames2[0] = Flame(x,y,z-2*r,r2,color2);     
    this->flames2[1] = Flame(x-2*r,y,z,r2,color2);     
    this->flames2[2] = Flame(x+2*r,y,z,r2,color2);     
    this->flames2[3] = Flame(x,y,z+2*r,r2,color2);     
    
}

void Fire::draw(glm::mat4 VP)
{
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (0 * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    int x = this->ticks_left/4;
    x = x%2;
    if(x==0)
    {
        for(int i=0; i<5; i++)
        {
            this->flames1[i].draw(VP);
        }
    }
    if(x==1)
    {
        for(int i=0; i<4; i++)
            this->flames2[i].draw(VP);
    }

}

void Fire::set_position(float x, float y)
{
    this->position = glm::vec3(x, y, 0);
}

void Fire::tick()
{
     this->ticks_left--;   
}
