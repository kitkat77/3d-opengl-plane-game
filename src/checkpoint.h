#include "main.h"

#ifndef CHECKPOINT_H
#define CHECKPOINT_H


class Checkpoint {
public:
    Checkpoint() {}
    Checkpoint(float x, float y,float z,color_t color);
    glm::vec3 position;
    float r;
    bool active;
    int cannons_left;
    bounding_box_t bound;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
private:
    VAO *object;
};

#endif // CHECKPOINT_H
