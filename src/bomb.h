#include "main.h"

#ifndef BOMB_H
#define BOMB_H


class Bomb {
public:
    Bomb() {}
    Bomb(float x, float y, float z,color_t color);
    glm::vec3 position;
    float rotation;
    float r;
    float x_r; // to scale x part 
    float y_r; // to scale y part
    float z_r; // to scale z part
    float acc_y;
    float speed_y;
    bool is_exist;
    void draw(glm::mat4 VP);
    void set_position(float x, float y,float z);
    void tick();
    bounding_box_t bound;
    void update_bounding_box();
private:
    VAO* object1; 
    VAO* object2; 
};

#endif // BOMB_H
