#include "arrow.h"
#include "main.h"

Arrow::Arrow(float x, float y, float z,color_t color)
{
    this->position = glm::vec3(x, y, z);
    this->angle = glm::vec3(0,0,0);
    this->width = 0.1;
    this->thickness = 0.5;
    this->bound.x = position.x;
    this->bound.y = position.y;
    this->bound.width = this->thickness;
    this->bound.height = this->width;
    
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    /*
    static const GLfloat vertex_buffer_data[] = {
        -this->width, this->thickness, 0.0f,
        this->width, this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        -this->width, this->thickness, 0.0f,
        -this->width, -this->thickness, 0.0f,
        this->width, -this->thickness, 0.0f,
        };
    */
    this->width *= 2;
    this->thickness *= 2;
    GLfloat vertex_buffer_data[] = {
        -this->width, 0.0f, this->thickness,
        this->width, 0.0f, this->thickness, 
        this->width, 0.0f, -this->thickness,
        -this->width, 0.0f, this->thickness,
        -this->width, 0.0f, -this->thickness,
        this->width, 0.0f, -this->thickness,

        /*
        -1.5*this->width, 0.0f, this->width+this->thickness,
        1.5*this->width, 0.0f, this->width+this->thickness,
        1.5*this->width, 0.0f, -this->width+this->thickness,
        -1.5*this->width, 0.0f, this->width+this->thickness,
        -1.5*this->width, 0.0f, -this->width+this->thickness,
        1.5*this->width, 0.0f, -this->width+this->thickness,
        */
        0.0f, 0.0f, this->width+this->thickness,
        0.0f, 0.0f, this->width+this->thickness,
        2*this->width, 0.0f, -this->width+this->thickness,
        0.0f, 0.0f, this->width+this->thickness,
        -2*this->width, 0.0f, -this->width+this->thickness,
        2*this->width, 0.0f, -this->width+this->thickness,
        };
    
    this->object = create3DObject(GL_TRIANGLES, 4*3, vertex_buffer_data, color , GL_FILL);
}

void Arrow::draw(glm::mat4 VP,glm::vec3 cur,glm::vec3 goal)
{
    //change in x : change in drawing of x
    //change in z : change in drawing of y
    //printf("%f %f\n",this->position.x,this->position.y);
    glm::vec3 v = glm::vec3(goal.x-cur.x,0,1*(goal.z-cur.z));
    //atan(y/x)
    if(length(v)<5)
    {
//        printf("found\n");
        return;
    }
    /*
    if(v.y==0)
    {
        if(v.x<0)
            this->angle.z = 180;
    }
    else if(v.x==0)
    {
        if(v.y>0)
            this->angle.z = 90;
        else
            this->angle.z = -90;
    }*/
    else
    {
        this->angle.y = 180*atan2(v.x,v.z)/M_PI;
    }

    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate_x  = glm::rotate((float) (this->angle.x * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 rotate_y  = glm::rotate((float) (this->angle.y * M_PI / 180.0f), glm::vec3(0, 1, 0));
    glm::mat4 rotate_z  = glm::rotate((float) (this->angle.z * M_PI / 180.0f), glm::vec3(0, 0, 1));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    //rotate          = rotate * glm::translate(glm::vec3(this->position.x,this->position.y,this->position.z));
    Matrices.model *= (translate * rotate_y * rotate_x *rotate_z);
    glm::mat4 MVP = VP * Matrices.model;
    
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Arrow::set_position(float x, float y)
{
    this->position = glm::vec3(x, y, 0);
}

/*
// to set parameters needed for the second rotation (to fix the score)
void Arrow::set_rotation(glm::vec3 axis,float angle,glm::vec3 axis_point)
{
    this->rotation2 = angle;
    //printf("%f\n",angle);
    this->axis = axis;
    this->axis_point = axis_point;

}
*/

void Arrow::tick()
{
        
}
