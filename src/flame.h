#include "main.h"

#ifndef FLAME_H
#define FLAME_H


class Flame {
public:
    Flame() {}
    Flame(float x, float y,float z,float r,color_t color);
    glm::vec3 position;
    float r;
    bounding_box_t bound;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
private:
    VAO *object;
};

#endif // FLAME_H
