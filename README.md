Assignment 2
=============

A 3D game implemented using OpenGL


##Objective

To destroy all cannons(enemies) on a checkpoint and move to the next checkpoint till we reach the final one


##Controls

- `W` : increase speed

- `Z` : decrease speed

- `A` : tilt left

- `D` : tilt right

- `Q` : rotate left (yaw)

- `E` : rotate right (yaw)

- `P` : move target of plane vertically upward

- `L` : move target of plane vertically downward

- `J` : move target of plane nearer to the plane

- `K` : move target of plane further to the plane

- `X` : to exit game

- `right click` : to drop bombs

- `left click` : to shoot missiles


##Views

To activate a view press :

- `1` : follow cam view

- `2` : top view

- `3` : tower view

- `4` : side view

- `5` : helicopter view


##Displays on Screen

- Altitude : bottom most in left corner
- Speed : above altitude
- Health : green bar
- Fuel : red bar
- Score : bottom right corner
- Compass : top right corner

##Features implemented

- Checkpoints(only one is active at a time)
- Cannons (shoot missiles at the plane)
- Missiles to be shot by plane
- Bombs that can be dropped by plane
- Arrow that points to the currently active checkpoint
- Volcanos that create a no-fly zone(death when in the area)
- Fuel-ups that refuel the tank
- Health that decreases when a missile from cannon hits the plane


##Bonus features
- Compass (top-right corner of screen)
- Fire (created at spots on checkpoints when hit by bombs or missiles)


##Crash conditions
- If altitude = 0; plane will crash into the sea
- If fuel = 0
- If health = 0
- If plane goes to a no-fly zone (created by volcano)
